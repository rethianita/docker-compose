const express = require('express'),
  path = require('path'),
  helpers = require('handlebars-helpers'),
  ehandlebars = require('express-handlebars'),
  session = require('express-session'),
  errorMiddleware = require('./middleware/error'),
  router = require('./routes/requests'),
  apiRoutes = require('./api');

const staticDir = path.join(__dirname, 'static');
const viewDir = path.join(__dirname, 'views');

const app = express();

// beállítjuk a handlebars-t, mint sablonmotor
app.set('view engine', 'hbs');
app.set('views', viewDir);

app.engine('hbs', ehandlebars({
  extname: 'hbs',
  defaultView: 'main',
  layoutsDir: path.join(__dirname, 'views/layouts'),
  partialsDir: path.join(__dirname, 'views/partials'),
  helpers: helpers(),
}));

app.use('/static', express.static(staticDir));

app.use(session({
  secret: '142e6ecf42884f03',
  resave: false,
  saveUninitialized: true,
}));

app.use('/api', apiRoutes);

app.use(express.urlencoded({ extended: true }));
app.use('/', router);
app.use(errorMiddleware);

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/'); });
