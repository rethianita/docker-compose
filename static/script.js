// kerdes aszinkron torlese
function submitDelete(qid) {
  // kivesszük a form adatait
  console.log(qid);
  fetch('/api/delete_question', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      kerdesID: qid,
    }),
  }).then((response) => {
    if (!response.ok) {
      console.log('Hiba');
      return -1;
    }
    return response.json();
  }).then((responseText) => {
    if (responseText === -1) {
      console.log('Hiba az aszinkron torlesben');
    } else {
      document.getElementById(`${responseText}`).innerHTML = '';
      console.log('Sikeres aszinkron torles.');
    }
  });
}

function expand(qid) {
  console.log(qid);
  const list = document.getElementById(`kerdes${qid}`);
  if (list.innerHTML !== '') {
    list.innerHTML = '';
  } else {
    fetch('/api/getAnswers', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        kerdesID: qid,
      }),
    }).then((res) => res.json())
      .then((data) => {
        const { length } = data;
        list.innerHTML = '';
        for (let i = 0; i < length; i += 1) {
          const newListElement = document.createElement('li');
          newListElement.innerHTML = data[i].valasz;
          list.appendChild(newListElement);
          console.log('Valaszok megjelenitve.');
        }
      });
  }
}
