function addAnswer() {
  const count = document.getElementById('wrongAnswer').value;
  const wrongAnswers = document.getElementById('wrongAnswers');

  for (let i = 1; i <= count; i += 1) {
    const label = document.createElement('label');
    label.innerHTML = `Helytelen válasz ${i}: `;
    const newAnswer = document.createElement('input');
    newAnswer.type = 'text';
    newAnswer.id = `wrongAnswer${i}`;
    newAnswer.name = `wrongAnswer${i}`;
    newAnswer.required = true;
    wrongAnswers.appendChild(label);
    wrongAnswers.appendChild(newAnswer);
    wrongAnswers.appendChild(document.createElement('p'));
  }
}

const wrongAnswer = document.getElementById('wrongAnswer');
wrongAnswer.addEventListener('blur', addAnswer);
