const url = require('url');

// segédfüggvény: minden hívásnak beállítja a teljes URL-jét
// segít a HATEOAS kialakításában
module.exports = (req, res, next) => {
  req.fullUrl = url.format({
    protocol: req.protocol,
    host: req.get('host'),
    pathname: req.originalUrl,
  });

  next();
};
