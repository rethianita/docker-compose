module.exports = (req, res) => {
  // kirajzoljuk a hibaoldalunk sablonját
  res.status(404).render('error', { message: 'The requested endpoint is not found' });
};
