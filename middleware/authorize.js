module.exports = function authorize(roles = ['user', 'admin']) {
  return (req, res, next) => {
    if (!req.session.role) {
      // a felhasználó nincs bejelentkezve
      console.log('You are not logged in');
      res.status(401).render('error', { message: 'Jelentkezz be, hogy elérd ezt a lehetőséget.' });
    } else if (!roles.includes(req.session.role)) {
      // a felhasználó be van jelentkezve de nincs joga ehhez az operációhoz
      console.log('You do not have permission to access this endpoint');
      res.status(401).render('error', { message: 'Csak adminok számára hozzáférhető.' });
    } else {
      next();
    }
  };
};
