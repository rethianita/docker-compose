const express = require('express'),
  dbquestion = require('../db/questions'),
  validate = require('../middleware/validate');

const router = express.Router();

// összes entitás lekérése
router.get('/', async (req, res) => {
  console.log('Osszes kerdes visszateritve.');
  dbquestion.findAllQuestions()
    .then((users) => res.json(users))
    .catch((err) => res.status(500).json({ message: `Error while finding all users: ${err.message}` }));
});

// összes entitás lekérése egy feltétel alapján, melyet query paraméterként adunk meg
// kerdesek lekerese szint alapjan
router.get('/level/:levelName', (req, res) => {
  const { levelName } = req.params;
  dbquestion.findAllQuestionsWithLevel(levelName)
    .then((questions) => (questions ? res.json(questions) : res.status(404).json({ message: `Question with level ${levelName} not found.` })))
    .catch((err) => res.status(500).json({ message: `Error while finding question with level ${levelName}: ${err.message}` }));
});

// entitás lekérése ID alapján
router.get('/:questionId', (req, res) => {
  const { questionId } = req.params;
  dbquestion.findQuestionById(questionId)
    .then((user) => (user ? res.json(user) : res.status(404).json({ message: `Question with ID ${questionId} not found.` })))
    .catch((err) => res.status(500).json({ message: `Error while finding question with ID ${questionId}: ${err.message}` }));
});

// entitás beszúrása
router.post('/', validate.hasProps(['leiras', 'szint', 'helyes', 'helytelen', 'ido']), async (req, res) => {
  const { helytelen } = req.body;
  dbquestion.insertQuestion(req)
    .then((question) => {
      for (let i = 1; i <= helytelen; i += 1) {
        dbquestion.insertAnswers(question.id, req.body[`wrongAnswer${i}`]);
      }
      dbquestion.insertRightAnswer(question.id, req.body);
      res.json(`Question with ID ${question.id} inserted.`);
    })
    .catch((err) => res.status(500).json({ message: `Error while inserting question: ${err.message}` }));
});

// entitás törlése
router.delete('/:questionId', (req, res) => {
  const { questionId } = req.params;
  dbquestion.findQuestionById(questionId).then((q) => {
    if (q.length > 0) console.log(q);
    else res.status(500).json({ message: `Question with ID ${questionId} does not exists.` });
  });
  Promise.all([dbquestion.deleteQuestionById(questionId),
    dbquestion.deleteAnswersById(questionId)])
    .then(() => res.json({ message: `Question with ID ${questionId} deleted` }))
    .catch((err) => res.status(500).json({ message: `Error while deleting question with ID ${questionId}: ${err.message}` }));
});

// entitás módosítása (akár teljes, akár részleges módon)
// tulajdonos megvaltoztatasa kerdesnel
router.patch('/:questionId', (req, res) => {
  const { questionId } = req.params;
  dbquestion.updateQuestion(questionId, req.body)
    .then((rows) => (rows ? res.sendStatus(204) : res.status(404).json({ message: `Question with ID ${questionId} not found.` })))
    .catch((err) => res.status(500).json({ message: `Error while updating question with ID ${questionId}: ${err.message}` }));
});


module.exports = router;
