const express = require('express'),
  bodyParser = require('body-parser'),
  // fullUrlMiddleware = require('../middleware/fullurl'),
  db = require('../db/questions'),
  promises = require('../routes/promises'),
  questionRoutes = require('./questions');

const router = express.Router();

// tároljuk minden hívás teljes URL-jét
// a HATEOAS kialakításáért
// router.use(fullUrlMiddleware);

// minden testtel ellátott API hívás
// JSON-t tartalmaz
router.use(bodyParser.json());

router.use('/questions', questionRoutes);

router.post('/delete_question', async (req, res) => {
  console.log('szija');
  const { kerdesID } = req.body;
  console.log(kerdesID);
  const kerdes = (await db.findQuestionById(kerdesID))[0];
  if (kerdes.tulajdonos !== req.session.username) {
    res.status(422).end(JSON.stringify('Csak tulajdonos torolheti a kerdest.'));
    return;
  }
  const deleteQuestionsPromise = promises.deleteQuestionPromise(kerdesID);
  const deleteAnwersPromise = promises.deleteAnswerPromise(kerdesID);
  Promise.all([deleteQuestionsPromise, deleteAnwersPromise])
    .then((resolve) => {
      console.log(resolve);
      res.set('Content-Type', 'application/json');
      res.end(JSON.stringify(kerdesID));
    })
    .catch((err) => {
      res.status(422).end(JSON.stringify(err.message));
    });
});

router.post('/getAnswers', async (req, res) => {
  const qid = req.body.kerdesID;
  const answers = await db.findAllAnwersByQuestionId(qid);
  res.set('Content-Type', 'application/json');
  res.end(JSON.stringify(answers));
});

module.exports = router;
