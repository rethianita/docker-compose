const crypto = require('crypto'),
  db = require('../db/users');

const hashSize = 32,
  saltSize = 16,
  hashAlgorithm = 'sha512',
  iterations = 1000;

exports.signUp = (req, res) => {
  const { password } = req.body;
  if (password !== req.body.passwordAgain) {
    res.status(500).render('error', { message: 'Passwords do not match' });
    return;
  }
  try {
    const salt = crypto.randomBytes(saltSize);

    crypto.pbkdf2(password, salt, iterations, hashSize, hashAlgorithm, (cryptErr, hash) => {
      if (cryptErr) {
        res.status(500).render('error', { message: `Hashing unsuccessful: ${cryptErr.message}` });
      } else {
        const hashWithSalt = Buffer.concat([hash, salt]).toString('hex');
        // a konkatenált hash-t és sót tárolnánk adatbázisban
        db.insertUser(req.body, hashWithSalt, (error) => {
          if (error) {
            console.log(error.message);
            res.status(500).render('error', { message: 'Mar van user ilyen felhasznalonevvel' });
          } else {
            console.log('User added.');
            res.redirect('/');
          }
        });
      }
    });
    // hash készítése
  } catch (err) {
    res.status(500).render('error', { message: 'Salt error' });
  }
};

exports.logIn = (req, res) => {
  const { username, password } = req.body;
  // le kell ellenorizni ha letezik user ilyen passworddal
  db.findUserByUsername(username, (err, user) => {
    if (err) {
      console.log(err);
    } else if (user[0]) {
      const hashWithSalt = user[0].Jelszo;
      const expectedHash = hashWithSalt.substring(0, hashSize * 2),
        salt = Buffer.from(hashWithSalt.substring(hashSize * 2), 'hex');
      crypto.pbkdf2(password, salt, iterations, hashSize, hashAlgorithm, (error, binaryHash) => {
        if (error) {
          res.status(500).send(`Hashing unsuccessful: ${err.message}`);
        } else {
          const actualHash = binaryHash.toString('hex');

          if (expectedHash === actualHash) {
            // bejelentkezees
            console.log('Sikeres bejelentkezes!');
            req.session.username = username;
            req.session.role = user[0].Szerep;
            res.redirect('/');
          } else {
            res.status(401).render('error', { message: 'Passwords do not match' });
          }
        }
      });
    } else {
      res.render('error', { message: 'Incorrect or unexisting username.' });
    }
  });
};

exports.logOut = (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      res.status(500).render('error', { message: `Session reset error: ${err.message}` });
    } else {
      res.redirect('/');
      console.log('Sikeres kijelentkezes.');
    }
  });
};
