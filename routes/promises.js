const db =  require('../db/questions');
const dbquiz = require('../db/quizes');

exports.deleteQuestionPromise = function deleteQuestionPromise(qid) {
  return new Promise((resolve, reject) => {
    db.deleteQuestionById(qid, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve('Kérdések sikeresen törölve.');
      }
    });
  });
};

exports.deleteAnswerPromise = function deleteAnswerPromise(qid) {
  return new Promise((resolve, reject) => {
    db.deleteAnswersById(qid, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve('Válaszok sikeresen törölve');
      }
    });
  });
};

exports.insertQuestionPromise = function insertQuestionPromise(req) {
  return new Promise((resolve, reject) => {
    db.insertQuestion(req, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve('Kérdés sikeresen beszúrva.');
      }
    });
  });
};

exports.getInsertedQuestionPromise = function getInsertedQuestionPromise() {
  return new Promise((resolve, reject) => {
    db.findInsertedQuestionId((err, qid) => {
      if (err) {
        reject(err);
      } else {
        resolve(qid);
      }
    });
  });
};

exports.insertRightAnswerPromise = function insertRightAnswerPromise(qid, req) {
  return new Promise((resolve, reject) => {
    db.insertRightAnswer(qid, req, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve('Helyes válasz sikeresen beszúrva!');
      }
    });
  });
};

exports.insertWrongAnswersPromise = function insertWrongAnswersPromise(qid, answer) {
  return new Promise((resolve, reject) => {
    db.insertAnswers(qid, answer, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve('Helytelen válasz beszúrva!');
      }
    });
  });
};

exports.insertQuizPromise = function insertQuizPromise(szint, kerdesekSzama) {
  return new Promise((resolve, reject) => {
    dbquiz.insertQuiz(szint, kerdesekSzama, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve('Kvíz sikeresen beszúrva.');
      }
    });
  });
};

exports.getInsertedQuizPromise = function getInsertedQuizPromise() {
  return new Promise((resolve, reject) => {
    dbquiz.findInsertedQuizId((err, qid) => {
      if (err) {
        reject(err);
      } else {
        resolve(qid);
      }
    });
  });
};

exports.insertQuestionToQuizPromise = function insertQuestionToQuizPromise(quizId, questionId) {
  return new Promise((resolve, reject) => {
    dbquiz.insertQuestionToQuiz(quizId, questionId, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve('Kérdés beszúrva a Kvízbe');
      }
    });
  });
};
