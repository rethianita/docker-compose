// hivasok lekezelese
const { check, validationResult } = require('express-validator');
const express = require('express'),
  auth = require('./auth'),
  promises = require('./promises'),
  db = require('../db/questions'),
  db2 = require('../db/quizes'),
  authorize = require('../middleware/authorize');

const router = express.Router();

// authentification
router.get('/signup', (req, res) => {
  res.redirect('/static/signup.html');
});

router.post('/add_user', [
  check('username').not().isEmpty().withMessage('nincs felhasznalonev'),
  check('email').not().isEmpty().withMessage('nincs email'),
  check('email').isEmail().withMessage('nem valid email cim'),
  check('password').not().isEmpty().withMessage('nincs jelszo'),
  check('passwordAgain').not().isEmpty().withMessage('nincs jelszo megerosites'),
], (req, res) => {
  auth.signUp(req, res);
});

router.post('/login', (req, res) => { auth.logIn(req, res); });

router.get('/logout', (req, res) => { auth.logOut(req, res); });

// kerdesek megjelenitese (mindenki szamara elerheto)
async function loadAnswers(question) {
  return {
    ...question,
    answers: await db.findAllAnwersByQuestionId(question.kerdesID),
  };
}

router.get('/', async (req, res) => {
  const questions = await Promise.all((await db.findAllQuestions())
    .map(loadAnswers));
  if (!req.session.role) {
    res.render('questions', { questions });
  } else {
    res.render('questions', { questions, logged: req.session.username });
  }
  console.log('Kérdések megjelenítve.');
});

// csak admin adhat hozza uj kerdest
router.get('/new_question', authorize(['admin']), (req, res) => {
  res.redirect('/static/index.html');
});

router.get('/new_quiz', authorize(), (req, res) => {
  res.redirect('/static/quizform.html');
});

// kerdes beszurasa/ torlese -> csak admin
router.post('/static/submit_form', [
  check('leiras').not().isEmpty().withMessage('nincs leírás'),
  check('szint').not().isEmpty().withMessage('válaszd ki a szintet'),
  check('szint').isIn(['konnyu', 'kozepes', 'nehez']).withMessage('nem létező szintet adtál meg'),
  check('helyes').not().isEmpty().withMessage('egy helyes válasz kötelező'),
  check('helytelen').not().isEmpty().withMessage('helytelen válasz üres'),
  check('helytelen').isInt({ min: 1 }).withMessage('szám kell legyen'),
], async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.send(422).render('error', { message: errors });
    return;
  }
  try {
    console.log(req.session.username);
    const insertPromise = await promises.insertQuestionPromise(req);
    console.log(insertPromise);
    const insertedId = (await promises.getInsertedQuestionPromise())[0].insertId;
    const insertRightAnswer = await promises.insertRightAnswerPromise(insertedId, req.body);
    console.log(insertRightAnswer);
    const { helytelen } = req.body;
    const valaszok = [];
    for (let i = 1; i <= helytelen; i += 1) {
      valaszok.push(req.body[`wrongAnswer${i}`]);
    }
    valaszok.forEach(async (valasz) => {
      const insertWrongAnswer = await promises.insertWrongAnswersPromise(insertedId, valasz);
      console.log(insertWrongAnswer);
    });
    res.redirect('/');
    console.log('Kérdés sikeresen beszúrva!');
  } catch (err) {
    console.log(err.message);
    res.status(500).render('error', { message: `${err}` });
  }
});

// uj kviz letrehozasa -> bejelentkezett felhasznalo
async function insertQuiz(szint, kerdesekSzama, questions, req, res) {
  try {
    const insertQuizMessage = await promises.insertQuizPromise(szint, kerdesekSzama);
    console.log(insertQuizMessage);
    const quizId = (await promises.getInsertedQuizPromise())[0].insertId;
    questions.forEach(async (kerdes) => {
      const insertQuestionMessage = await
      promises.insertQuestionToQuizPromise(quizId, kerdes.kerdesID);
      console.log(insertQuestionMessage);
    });
    if (!req.session.role) {
      res.render('quizes', { questions, submited: false, quizId });
    } else {
      res.render('quizes', {
        questions, submited: false, quizId,  logged: req.session.username,
      });
    }
  } catch (err) {
    res.status(500).render('error', { message: `Sikertelen törlés: ${err.message}` });
  }
}


router.post('/static/new_quiz', [
  check('kerdesSzam').not().isEmpty().withMessage('kérdések száma nem lehet üres'),
  check('kerdesSzam').isInt({ min: 1 }).withMessage('kérdések száma nem numerikus2'),
  check('szint').not().isEmpty().withMessage('válassz nehézségi szintet'),
  check('szint').isIn(['konnyu', 'kozepes', 'nehez']).withMessage('nem létező szintet adtál meg'),
], async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.render('error', { message: errors.message });
    return;
  }
  // uj quiz letrehozasa -> betoltese
  // kivalasztjuk az adott nehezsegu szintu kerdeseket
  const questionsWithLevel = await db.findAllQuestionsWithLevel(req.body.szint);
  let questions = await Promise.all(questionsWithLevel.map(loadAnswers));
  console.log(questions);
  const kerdesekSzama = req.body.kerdesSzam;
  console.log(questions.length);
  if (kerdesekSzama > questions.length) {
    res.status(422).render('error', { message: 'Nincs elég kérdés az adatbázisban.' });
    return;
  }
  for (let i = questions.length - 1; i > 0; i -= 1) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = questions[i];
    questions[i] = questions[j];
    questions[j] = temp;
  }
  questions = questions.slice(0, kerdesekSzama);
  for (let i = 0; i < kerdesekSzama; i += 1) {
    const valaszok = questions[i].answers;
    // ezt is shuffle-oljuk
    for (let k = valaszok.length - 1; k > 0; k -= 1) {
      const j = Math.floor(Math.random() * (k + 1));
      const temp = valaszok[k];
      valaszok[k] = valaszok[j];
      valaszok[j] = temp;
    }
  }
  console.log('Kvíz sikeresen generálva!');
  insertQuiz(req.body.szint, kerdesekSzama, questions, req, res);
});

router.post('/static/submit_quiz', async (req, res) => {
  console.log('Kviz kitoltve!');
  const quizId = req.body.kvizId;
  const questions = await Promise.all((await db2.findAllQuestionsById(quizId))
    .map(loadAnswers));
  for (let i = 0; i < questions.length; i += 1) {
    const valasz = req.body[`valasz${questions[i].kerdesID}`];
    const helyes = (questions[i].answers.filter((answer) => answer.helyes === 1));
    const helyesValasz = helyes[0].valasz;
    let value = false;
    if (valasz === helyesValasz) {
      value = true;
    }
    questions[i].eltalalta = value;
  }
  if (!req.session.role) {
    res.render('quizes', { questions, submited: true, quizId });
  } else {
    res.render('quizes', {
      questions, submited: true, quizId,  logged: req.session.username,
    });
  }
  console.log('Kérdések megjelenítve.');
});

module.exports = router;
