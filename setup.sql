-- uj adatbazis letrehozasa
drop database Kvizek;
CREATE DATABASE IF NOT EXISTS Kvizek;

-- készít egy felhasználót, aki minden műveletet végrehajthat ezen adatbázisban
USE Kvizek;

create user if not exists user identified by '1234';
GRANT ALL PRIVILEGES ON *.* TO user;

create table if not exists Kerdesek (
kerdesID int not null auto_increment,
szint varchar(10),
leiras varchar(256),
ido int,
tulajdonos varchar(50),
primary key(kerdesID)
);

create table if not exists Valaszok (
kerdesID int references Kerdesek(kerdesID),
valasz varchar(256),
helyes boolean
);

create table if not exists Felhasznalok (
felhId int not null auto_increment,
Email varchar(50),
FelhNev varchar(50) unique,
Jelszo varchar(2000),
Szerep varchar(20),
primary key(felhId)
);

create table if not exists Quizek (
quizID int not null auto_increment,
szint varchar(10),
kerdesekSzama int,
primary key(quizID)
);

create table if not exists QuizKerdesek (
quizID int references Quizek(quizID),
kerdesID int references Kerdesek(kerdesID)
);


insert into Kerdesek(szint, leiras, tulajdonos, ido) values ("konnyu", "Honnan tudjuk meg egy állomány méretét ext2 filerendszerben?", "admin", 22);
insert into Kerdesek(szint, leiras, tulajdonos, ido) values ("konnyu", "Az ext2 filerendszerben az állományok neve, mérete és a hozzáférési jogok az inode táblázatban vannak nyilvántartva.", "admin", 60);
insert into Kerdesek(szint, leiras, tulajdonos, ido) values ("konnyu", "Az alábbiak közül mi nem található a szuperblokkban:", "admin", 60);
insert into Valaszok values (1,'a directorybemenetből',false);
insert into Valaszok values (1,'kivonjuk a fie utolsó byteja utáni byte címéből a file kezdőcímét',false);
insert into Valaszok values (1,'a méret szerepel az i-nodeban',true);
insert into Valaszok values (1,'a du parancs segítségével',false);
insert into Valaszok values (2,'Igaz',false);
insert into Valaszok values (2,'Hamis',true);
insert into Valaszok values (3,"a felhasználó csoportazonosítója", true);
insert into Valaszok values (3,"a szabad blokkok száma", false);
insert into Valaszok values (3,"az inode bitmap blokkok száma", false);
insert into Valaszok values (3,"az inode tábla kezdő blokkjának száma", false);

insert into Felhasznalok(Email, FelhNev, Jelszo, Szerep) values("admin@yahoo.com","admin","0a6e4dfd131c1946ff6bc8e6260a9a32eadf816d69a4dbf6d9951e1dabd3196635131f91bf73b6cfa545a8aa074096c7", "admin");