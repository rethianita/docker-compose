// adatbazis muveletek
const mysql = require('mysql2');

// kapcsolodas:
const pool = mysql.createPool({
  connectionLimit: 10,
  database: 'Kvizek',
  host: 'db',
  port: 3306,
  user: 'user',
  password: '1234',
});// .promise();

/* module.exports = async function runQuery(query, options = []) {
  return (await pool.query(query, options))[0];
}; */

module.exports = (query, options = []) => new Promise((resolve, reject) => {
  pool.query(query, options, (error, results) => {
    if (error) {
      reject(new Error(`Error while running '${query}: ${error}'`));
    }
    resolve(results);
  });
});
