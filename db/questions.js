const runQuery = require('./config');

// select
exports.findAllQuestions = () => runQuery('SELECT * FROM Kerdesek');
exports.findAllAnwersByQuestionId = (questionId) => runQuery('SELECT * FROM Valaszok where kerdesID=?', [questionId]);
exports.findAllQuestionsWithLevel = (level) => runQuery('select * from Kerdesek where szint=?', [level]);

exports.insertQuestion = (request) => runQuery(`INSERT INTO Kerdesek(szint, leiras, ido, tulajdonos) VALUES (
    "${request.body.szint}", "${request.body.leiras}","${request.body.ido}", "${request.session.username}");`)
  .then((result) => ({ id: result.insertId }));

// insert
exports.findQuestionById = (qid) => runQuery(`select * from Kerdesek where kerdesID=${qid}`);

exports.findInsertedQuestionId = (callback) => {
  runQuery('select max(kerdesID) as insertId from Kerdesek', callback);
};

exports.insertRightAnswer = (qid, request, callback) => {
  const query = `insert into Valaszok values ("${qid}", "${request.helyes}", true);`;
  runQuery(query, callback);
};

exports.insertAnswers = (qid, ertek, callback) => {
  runQuery(`INSERT INTO Valaszok VALUES (
    "${qid}", "${ertek}", false);`, callback);
};

// delete
exports.deleteQuestionById = (qId, callback) => {
  runQuery(`DELETE FROM Kerdesek WHERE kerdesID = "${qId}"`, callback);
};

exports.deleteAnswersById = (qId, callback) => {
  runQuery(`DELETE FROM Valaszok where kerdesID = "${qId}"`, callback);
};

// update
exports.updateQuestion = (qId, req) => runQuery('UPDATE Kerdesek SET tulajdonos=? WHERE kerdesID=?', [req.username, qId])
  .then((result) => result.affectedRows > 0);
