const runQuery = require('./config');

exports.insertQuiz = (szint, kerdesekSzama, callback) => {
  runQuery(`insert into Quizek(szint, kerdesekSzama) values("${szint}","${kerdesekSzama}");`, callback);
};

exports.findInsertedQuizId = (callback) => {
  runQuery('select max(quizID) as insertId from Quizek;', callback);
};

exports.insertQuestionToQuiz = (quizId, questionId, callback) => {
  runQuery(`insert into QuizKerdesek values ("${quizId}", "${questionId}")`, callback);
};

exports.findAllQuestionsById = (quizId) => runQuery('select Kerdesek.kerdesID, szint, leiras, ido from QuizKerdesek join Kerdesek on Kerdesek.kerdesID = QuizKerdesek.kerdesID where quizID=?', [quizId]);
